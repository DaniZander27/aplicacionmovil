package facci.danielcevallos.convertidortemperatura;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText cantidad = null;
    Spinner seleccion = null;
    Button convertir = null;
    TextView resultado = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cantidad = (EditText)findViewById(R.id.canti);
        seleccion = (Spinner)findViewById(R.id.spinseleccion);
        convertir = (Button)findViewById(R.id.btnconvertir);
        resultado = (TextView)findViewById(R.id.resultado);

        String[] op = {"°C - °F", "°F - °C"};
        ArrayAdapter<String> adapter = new
                ArrayAdapter<String> (this, android.R.layout.simple_spinner_item,op);
        seleccion.setAdapter(adapter);

        convertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cantidad.getText().toString().equals("")){
                    Toast mensaje = Toast.makeText(getApplicationContext(), "Escribe una cantidad", Toast.LENGTH_LONG);
                    mensaje.show();
                }else {
                    Double c = Double.parseDouble(cantidad.getText().toString());
                    Double res = null;
                    int opcion = seleccion.getSelectedItemPosition();

                    switch (opcion){
                        case 0:
                            res = 0.0;
                            Toast.makeText(getApplicationContext(),"Selecciona una opcion", Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            res = 1.8 * c + 32;
                            break;
                        case 2:
                            res = (c - 32)/1.8;
                            break;
                    }
                    resultado.setText(res.toString());
                }
            }
        });
    }
}
